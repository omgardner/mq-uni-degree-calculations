# README #

This is a repo to hold a xlsx file to calculate credit points remaining in a degree.

This has only been tested on a single-major bachelor degree for Macquarie University. It may require adjusting to make it compatible with other universities/postgrad/double major/etc.